Kvitter::Application.routes.draw do

  root :to => "messages#index"

  get 'kvitter' => 'messages#index'

  get 'admin' => 'messages#admin'

  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    get 'logout' => :destroy # TODO why delete 'logout'... in example
  end

  resources :users

  resources :messages

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"
end
