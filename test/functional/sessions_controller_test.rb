require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

  test "should login" do
    test_user = users(:one)
    post :create, name: test_user.name, password: 'hemligt'
    assert_redirected_to root_url
    assert_equal test_user.id, session[:user_id]
  end

  test "should fail to login" do
    test_user = users(:one)
    post :create, name: test_user.name, password: 'felaktigt'
    assert_redirected_to login_url
  end

  test "should logout" do
    get :destroy              # TODO was delete: destroy ??
    assert_redirected_to login_url
  end

end
