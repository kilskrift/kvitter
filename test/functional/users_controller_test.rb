require 'test_helper'

class UsersControllerTest < ActionController::TestCase

  def generate_unique_name
    @i||=0 #instantiate iff not exist
    "test_user #{@i+=1}"
  end

  setup do
    @input_attributes = {
      :name => generate_unique_name,
      :password => 'test_pw',
      :password_confirmation => 'test_pw'
    }
    
    @user = users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post :create, user: @input_attributes
    end

    assert_redirected_to users_path
  end

  test "should show user" do
    get :show, id: @user.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user.to_param
    assert_response :success
  end

  test "should update user" do
    put :update, id: @user.to_param, user: @input_attributes
    assert_redirected_to users_path
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete :destroy, id: @user.to_param
    end

    assert_redirected_to users_path
  end
end
