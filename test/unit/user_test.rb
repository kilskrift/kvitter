require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test 'test_that_name_is_unique' do
    my_attributes = {
      :name => "not_unique_name",
      :password => "pw",
      :password_confirmation => "pw"
    }

    first_user = User.new my_attributes
    first_user.save

    assert !first_user.new_record?

    second_user = User.new my_attributes # new = create without save after

    assert !second_user.valid?
  end

end
