require 'digest/sha2'

# see agile web development w/rails 4th, p.184ff
#
class User < ActiveRecord::Base

  validates :name, :presence => true, :uniqueness => true
  validates :password, :confirmation => true

  validate :password_must_be_present # TODO read up on this
  
  after_destroy :ensure_an_admin_remains

  def ensure_an_admin_remains
    if User.count.zero?
      raise "Can't delete last user"
    end
  end

  attr_accessor :password_confirmation # create reader, writer for pw_confirmation

  attr_reader :password
  def password=( password ) # non-standard writer, store encrypted pw instead
    @password = password
    
    if password.present?
      generate_salt
      self.hashed_password = self.class.encrypt_password( password, salt )
    end

  end    
  
  def User.authenticate( name, password )
    if user = find_by_name( name )  # lookup user in db
      if user.hashed_password == encrypt_password( password, user.salt )
        user #return value
      end
    end
  end

  def User.encrypt_password( password, salt)
    Digest::SHA2.hexdigest( password + "wibble" + salt ) # wibble?
  end  
  
  private
  
  # TODO this puts up validation error if no hashed_pw present/generated??
  def password_must_be_present
    errors.add( :password, "Missing password" ) unless hashed_password.present?
  end

  def generate_salt
    self.salt = self.object_id.to_s + rand.to_s
  end

end
