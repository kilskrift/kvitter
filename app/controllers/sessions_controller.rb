class SessionsController < ApplicationController
 
  skip_before_filter :authorise

  def new
  end

  def create
    if user = User.authenticate( params[:name], params[:password])
      session[:user_id] = user.id
      #TODO redirect where?
      redirect_to root_url, alert: "Logged in"
    else
      redirect_to login_url, alert: "Invalid user/password combination"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to login_url, alert: "Logged out"
  end

end
